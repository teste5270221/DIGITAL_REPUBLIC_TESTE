import type { Config } from 'jest';

const config: Config = {
  bail: 1,
  verbose: true,
  testEnvironment: 'jest-environment-jsdom',
  setupFilesAfterEnv: ['<rootDir>/setupTests.ts'],
  transform: {
    '^.+\\.(js|jsx)$': 'babel-jest',
    '^.+\\.(ts|tsx)$': 'babel-jest',
  },
  transformIgnorePatterns: [
    "/node_modules/",
    "\\.pnp\\.[^\\\/]+$",
    "<rootDir>/src/components/.*\\.css"
  ],
  moduleNameMapper: {
    "^.+\\.css$": "identity-obj-proxy"
  }  
};

export default config;
