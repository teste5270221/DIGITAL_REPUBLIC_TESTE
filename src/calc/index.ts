const MIN_DIFF_ALTURA_PORTA_PAREDE = 0.3;
const PORTA_ALTURA = 1.90;
const PORTA_LARGURA =  0.80;
const AREA_JANELA = 2 * 1.2;
const AREA_PORTA = PORTA_ALTURA * PORTA_LARGURA;
const NUM_PAREDES_SALA = 4;
const MIN_ALTURA_PORTA = (10 * PORTA_ALTURA + 10 * MIN_DIFF_ALTURA_PORTA_PAREDE)/10;
const LATAS = [18, 3.6, 2.5, 0.5] as const;
type LataType = (typeof LATAS)[number];

function calcularMaxJanelas(areaParede: number, numPortas?: number): number {
  if (!numPortas) {
    return 0
  }
  const areaMaxAberturas = areaParede * 0.5;
  const areaUsadaPorPortas = numPortas * AREA_PORTA;
  const areaDisponivelParaJanelas = areaMaxAberturas - areaUsadaPorPortas;
  const maxJanelas = Math.floor(areaDisponivelParaJanelas / AREA_JANELA);
  return maxJanelas > 0 ? maxJanelas : 0;
}

function calcularMaxPortas(areaParede: number, numJanelas?: number): number {
  if (!numJanelas) {
    return 0
  }
  const areaMaxAberturas = areaParede * 0.5;
  const areaUsadaPorJanelas = numJanelas * AREA_JANELA;
  const areaDisponivelParaPortas = areaMaxAberturas - areaUsadaPorJanelas;
  const maxPortas = Math.floor(areaDisponivelParaPortas / AREA_PORTA);
  return maxPortas > 0 ? maxPortas : 0;
}


function calcularAreaParede(values: {altura?: number; largura?: number; numJanelas?: number; numPortas?: number;}) {
  if (!values.altura || !values.largura) {
    return;
  }
  const areaParede = values.altura * values.largura * NUM_PAREDES_SALA;
  const areaJanelas = (values.numJanelas || 0) * AREA_JANELA;
  const areaPortas = (values.numPortas || 0) * AREA_PORTA;
  const areaTotalAberturas = areaJanelas + areaPortas;
  return areaParede - areaTotalAberturas;
}

function calcularQuantidadeTinta(areaTotal: number): number {
  const quantidadeTinta = Number((areaTotal / 5).toFixed(2));
  if (quantidadeTinta < 0.5) {
    return 0.5;
  }
  return quantidadeTinta;
}


function determinarLatasTinta(quantidadeTinta: number) {
  const latasNecessarias = new Map<LataType, number>();

  LATAS.forEach((litros) => latasNecessarias.set(litros, 0))

  for (const lata of LATAS) {
      const numLatas = Number(Math.round(quantidadeTinta / lata).toFixed(2));
      if (numLatas > 0) {
        latasNecessarias.set(lata, latasNecessarias.get(lata) || 0 + numLatas)
        quantidadeTinta -= numLatas * lata;
      }
  }

  return latasNecessarias;
}

function calcularTinta(values: {altura?: number; largura?: number; numJanelas?: number; numPortas?: number;}) {
  const areaTotalParedes = calcularAreaParede(values);

  if (!areaTotalParedes) {
    return 'Impossível calcular sem dados válidos de altura e largura da parede'
  }

  const quantidadeTinta = calcularQuantidadeTinta(areaTotalParedes);
  const latasNecessarias = determinarLatasTinta(quantidadeTinta);

  const textoQuantidadeTinta = 'Serão necessárias: ';
  const quantidadeTintas: string[] = [];
  
  for (const [key, value] of latasNecessarias) {
    if (value) {
      quantidadeTintas.push(`${value} ${value > 1 ? 'latas' : 'lata'} de ${key}L`);
    }
  }

  return textoQuantidadeTinta + quantidadeTintas.join(' + ');
}

export {
  MIN_DIFF_ALTURA_PORTA_PAREDE,
  PORTA_ALTURA,
  PORTA_LARGURA,
  AREA_JANELA,
  AREA_PORTA,
  NUM_PAREDES_SALA,
  MIN_ALTURA_PORTA,
  calcularMaxJanelas,
  calcularMaxPortas,
  calcularTinta,
}