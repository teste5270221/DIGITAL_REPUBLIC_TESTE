import { useMemo, useState } from "react";
import { NumberInput } from "../components/numberInput";
import {
  NUM_PAREDES_SALA,
  calcularMaxJanelas,
  calcularMaxPortas,
  calcularTinta,
  MIN_ALTURA_PORTA
} from "../calc";
import './form.css';

export function FormParedes() {
const [altura, setAltura] = useState<number>();
const [largura, setLargura] = useState<number>();
const [numPortas, setNumPortas] = useState<number>();
const [numJanelas, setNumJanelas] = useState<number>();
const [maxLargura, setMaxLargura] = useState<number>();
const [texto, setTexto] = useState('');

const areaParede = useMemo(() => {
  if (!altura || !largura) {
    return 0;
  }
  return altura * largura * NUM_PAREDES_SALA
}, [altura, largura])

const maxPortas = useMemo(() => {
  return calcularMaxPortas(areaParede, numJanelas);
},[areaParede,numJanelas])

const maxJanelas = useMemo(() => {
  return calcularMaxJanelas(areaParede, numPortas);
},[areaParede,numPortas])

  return (
    <div className="mainContainer">
    <p>Calcule as latas necessárias para cada sala</p>
    <div className="mainForm">
      <NumberInput
        label="Altura das paredes"
        max={50/(largura || 1)}
        min={MIN_ALTURA_PORTA}
        handleChange={(value) => {
          setAltura(value);
          setMaxLargura(50/(value || 1));
        }}
      />
      <NumberInput
        label="Largura das paredes"
        max={maxLargura}
        min={1}
        handleChange={(value) => { setLargura(value) }}
      />
      <NumberInput
        label="Quantidade de portas"
        max={maxPortas}
        min={0}
        handleChange={(value) => { setNumPortas(value) }}
      />
      <NumberInput
        label="Quantidade de janelas"
        max={maxJanelas}
        min={0}
        handleChange={(value) => { setNumJanelas(value) }}
      />
    </div>
      <button disabled={!altura||!largura||!numPortas||!numJanelas} onClick={() => setTexto(calcularTinta({altura,largura,numJanelas,numPortas}))}>Calcular quantidade de tinta</button>
      {texto}
    </div>
  );

}