import { useEffect, useState } from 'react';
import './numbeInput.css'

type NumberInputProps = {
  label: string;
  min: number;
  max?: number;
  handleChange: (value?: number) => void
};

export function NumberInput({ label, min, max, handleChange }: NumberInputProps) {
  const [value, setValue] = useState<number | ''>('');

  useEffect(() => {
    const timeoutId = setTimeout(() => {
      if (value !== '' && value < min) {
        handleChange(min);
        setValue(min);
      } else if (value !== '' && max && value > max) {
        handleChange(max);
        setValue(max);
      }
    }, 500);

    return () => clearTimeout(timeoutId);
  }, [value, min, max, handleChange]);

  return (
    <label className="inputNumberLabel">
      {label}
      <input
        className='inputNumber'
        data-variant="default"
        type="number"
        value={value}
        onChange={({ target }) => {
          const inputValue = target.value;
          if (inputValue) {
            handleChange(Number(inputValue));
            setValue(Number(inputValue));
          } else {
            handleChange(undefined);
            setValue('');
          }
        }}
        min={min}
        max={max}
      />
    </label>
  );
}
