import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { FormParedes } from '../form';

describe('FormParedes', () => {
  test('renderiza corretamente os inputs', () => {
    render(<FormParedes />);
    
    expect(screen.getByLabelText(/Altura das paredes/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Largura das paredes/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Quantidade de portas/i)).toBeInTheDocument();
    expect(screen.getByLabelText(/Quantidade de janelas/i)).toBeInTheDocument();
  });

  test('calcula as tintas corretamente', async () => {
    render(<FormParedes />);

    const alturaInput = screen.getByLabelText(/Altura das paredes/i);
    const larguraInput = screen.getByLabelText(/Largura das paredes/i);
    const portasInput = screen.getByLabelText(/Quantidade de portas/i);
    const janelasInput = screen.getByLabelText(/Quantidade de janelas/i);
    const calculateButton = screen.getByRole('button', { name: /calcular quantidade de tinta/i });

    await userEvent.type(alturaInput, '3');
    await userEvent.type(larguraInput, '5');
    await userEvent.type(portasInput, '1');
    await userEvent.type(janelasInput, '2');

    await userEvent.click(calculateButton);

    expect(screen.getByText(/Serão necessárias: 1 lata de 18L/i)).toBeInTheDocument();
  });
});
